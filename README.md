# Steam Cypher Database

 This Repository contains all the code and material used for the second team project of the Database 2 course of the Computer Engineering Master Degree at Università  degli Studi di Padova.


Team Members:

- Tommaso Baldo
- Elia Detogni
- Andrea Frasson


## Brief Explanation of the Project

The intent of this project is to develop a Cypher Database containing a portion of data of Steam, a popular videogame distribuition service.

## Datasets
2 datasets were used for this project. The first one can be found at https://steam.internet.byu.edu and includes millions of users, games, stats about games, friendship relationships and groups, as well as data about game ownership. The zipped file alone is fairly large (17Gb) and reaches 170Gb unzipped. For this reason it was not included in the repository. Instead, it was ingested in its entirety in a DBMS and a subset of it, containing roughtly 1 million users and 3400 games, was created and stored.  

The second dataset can instead be found at https://www.kaggle.com/forgemaster/steam-reviews-dataset and contains data regarding game reviews left by users. Also this dataset is not present in the repository as is, it was instead ingested into a DBMS, cleaned and with a join operation only the reviews left by users present in the subset previously extracted were kept.


## Content of the repository
```
 |
 |- big-dataset-cleaned     A bigger dataset used for more specific queries
 |- data-analysis           Contains all graphs and images that will be used in the presentation of the Project
 |- data-expansion          Contains the notebooks used to expand the data using the Steam API
 |- final-dataset           A more compact version of the original dataset
 |
 |- graph                   Contains the Schema of the Database
 |- populate graph          Contains all notebooks and CQL files needed to populate the database
 |- queries                 Contains the developed queries 
     |-algorithm            Contains the queries that used specific search algorithms
     |-selected             Contains the 10 selected "standard" queries 

```

## Usage

It is possible to build two different databases:

- A small one that can be used to check that everything is correct.
- A bigger one on which we ran the queries. If you desire to get the same results of us, queries and algorithms have to be executed on this one.

We chose this approach because the big dataset requires some time to load (approximately 30 minutes on our hardware) and the queries and algorithms require some time to run.  
As such, the small dataset proved useful to speed up things while developing this project. 

For both options there are two possibilities for the ingestion phase:

- A Python Notebook **(if you want to test this you have to change the neo4j credentials in the notebook with yours)**
- A .cql file that can be pasted into neo4j browser.

Everything is contained in the "populate-graph" folder. The Jupyter notebooks are called **loadBigDataset.ipynb** and **loadSmallDataset.ipynb** respectively for the big and small datasets.  
The same naming scheme holds for the cql files. **IMPORTANT:** The Jupyter notebooks utilize the local datasets stored inside the relative folders. However, the cql files need to download the datasets from the repository in order to populate the database. For this reason it is hightly suggested to avoid populating the big version of the database using the relative cql file, utilizing instead the Jupyter notebook.

## Queries

All the queries are contained in their relative files inside the *queries* folder. The 10 selected queries are inside the *selected* folder while the *algorithm* folder contains code to run some graph algorithms on the database.   
Finally the selected queries can be tested by using the notebook named **queries-notebook.ipynb** which will connect to the database and display the results of each query.